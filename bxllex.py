import ply.lex as lex
import sys


tokens = (
    'STRING',
    'IDENTIFIER',
    'INTEGER',
    'DECIMAL',
    'COLON',
    'COMMA',
    'LPAREN',
    'RPAREN',
)

t_COLON = r':'
t_COMMA = r','
t_LPAREN = r'\('
t_RPAREN = r'\)'


def t_STRING(t):
    r'"[\d\w\-_/\.,\+\*\=\: \(\)\\]*"'
    t.value = t.value.replace('"', '')
    return t

t_IDENTIFIER = r'[\d\w\-_\/\.\+\*\=]+'

def t_DECIMAL(t):
    r'\-?\d*\.\d+'
    t.value = float(t.value)
    return t

def t_INTEGER(t):
    r'\-?\d+'
    t.value = int(t.value)
    return t


#t_WS = '[ \t]+'
#t_NL = '[\n]'


t_ignore  = ' \t'


def t_newline(t):
    r'\r?\n'
    t.lexer.lineno += len(t.value)

def t_error(t):
    print("Illegal character '%r' at line %d" % (t.value[0], t.lexer.lineno))
    raise Exception()

data='TextStyle "H50s3" (FontWidth 3) (FontHeight 50) (FontCharWidth 25)'

# Give the lexer some input
lexer = lex.lex()



# Compute column. 
#     input is the input text string
#     token is a token instance
def find_column(input, token):
    last_cr = input.rfind('\n',0,token.lexpos)
    if last_cr < 0:
        last_cr = 0
    column = (token.lexpos - last_cr) + 1
    return column



if __name__ == '__main__':
    lexer.input(open(sys.argv[1]).read())
    # Tokenize
    while True:
        tok = lexer.token()
        if not tok: 
            break      # No more input
        print(tok)


