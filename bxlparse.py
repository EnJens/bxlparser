import ply.yacc as yacc
import sys
from bxllex import tokens

def p_stmts_1(p):
    'stmts : stmt'
    p[0] = p[1]

def p_stmts_2(p):
    'stmts : stmts stmt'
    p[0] = [p[1], p[2]]

def p_stmts_3(p):
    'stmts : empty'
    pass

def p_stmt(p):
    '''stmt : endstmt
           | plainstmt
           | cntstmt
           | tlstmt
           | varstmt'''
    p[0] = p[1]

def p_empty(p):
    'empty :'
    pass

def p_tlstmt_1(p):
    'tlstmt : identifier number string vars'
    p[0] = {'type': p[1], 'id': p[2], 'name': p[3], 'vars': p[4]}

def p_tlstmt_2(p):
    'tlstmt : identifier string vars'
    p[0] = {'type': p[1], 'name': p[2], 'vars': p[3]}

def p_tlstmt_4(p):
    'tlstmt : identifier number vars'
    p[0] = {'type': p[1], 'id': p[2], 'vars': p[3]}

def p_tlstmt_3(p):
    'tlstmt : identifier vars'
    p[0] = {'type': p[1], 'vars': p[2]}


def p_endstmt(p):
    'endstmt : identifier'
    pass

def p_plainstmt(p):
    'plainstmt : identifier string'
    p[0] = {'type': p[1], 'name': p[2]}

def p_cntstmt(p):
    'cntstmt : identifier COLON number'
    p[0] = {'type': p[1], 'count': p[3]}

def p_cntstmt_1(p):
    'cntstmt : identifier number'
    p[0] = {'type': p[1], 'count': p[2]}


def p_varstmt(p):
    'varstmt : vars'
    p[0] = p[1]


def p_vars_1(p):
    '''vars : var'''
    p[0] = [p[1]]

def p_vars_2(p):
    '''vars : vars var'''
    p[0] = p[1]
    p[0].append(p[2])


def p_var(p):
    '''var : LPAREN identifier varspec RPAREN'''
    p[0] = {p[2]: p[3]}

def p_var_2(p):
    '''var : LPAREN varspec RPAREN'''
    p[0] = p[2]

def p_varspec_1(p):
    'varspec : varspec_value'
    p[0] = p[1]

def p_varspec_2(p):
    'varspec : varspec varspec_value'
    if p[1] is not list:
        p[1] = [p[1]]
    p[0] = p[1]
    p[0].append(p[2])

def p_varspec_3(p):
    'varspec : varspec COMMA varspec_value'
    if p[1] is not list:
        p[1] = [p[1]]
    p[0] = p[1]
    p[0].append(p[3])

def p_varspec_value_1(p):
    '''varspec_value : identifier
                      | string
                      | number'''
    p[0] = p[1]

def p_string(p):
    'string : STRING'
    p[0] = p[1]

def p_number(p):
    '''number : INTEGER 
              | DECIMAL'''
    p[0] = p[1]

specials = {
    'True': True,
    'False': False,
}

def p_identifier(p):
    'identifier : IDENTIFIER'
    if p[1] in specials:
        p[0] = specials[p[1]]
    else:
        p[0] = p[1]

def p_error(p):
    print("Syntax error in input! %r (%d)" % (p, p.lexer.lineno))
    raise Exception()

parser = yacc.yacc()

res = []
with open(sys.argv[1]) as f:
    for line in f:
        result = parser.parse(line, debug=False)
        if result:
            res.append(result)

import pprint
p = pprint.PrettyPrinter(indent=4)
p.pprint(res)
